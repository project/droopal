Droopal: Object Oriented Drupal
http://drupal.org/project/droopal
Drupal 6.x

## ABOUT ##

The Droopal module offers out of the box support for writing modules in full
OOP and accessing other modules via OOP. Here are some examples of what you
can do out of the box, all with zero configuration:

Access any core module using OOP:

    ModNode::load()

This would be the equivalent of:

    node_load()

Access any core include using OOP:

    LibCommon::drupal_add_css()

This would be the equivalent of:

    drupal_add_css();

Access any non-core module or include using OOP:

    ModImagecache::action_save

This would be the equivalent of:

    imagecache_action_save()

Write custom modules using OOP structures:

    class ModMyModule {
      // hook_menu
      function menu() {
        //etc...
      }
      // hook_nodeapi
      function nodeapi() {
        //etc...
      }
    }

The functionality to make this work is completely hidden and offered at
virtually no performance hit. All existing Drupal APIs also remain
completely intact.

## WARNINGS ##

  * This module will safely modify your core Drupal files.
  * Upon un-installation, this file will safely restore your core
    Drupal files, provided you follow the instructions below.
  * If any of the installation instructions below do not make sense to
    you, don't make assumptions. As with any module, know what you are
    doing before installing this module.
  * This module has only been designed to work on UNIX based systems.

INSTALLATION INSTRUCTIONS

  1. As always, backup your important files
  2. Make sure the "patch" utility is installed and accessible from your
     PHP scripts. This is VERY IMPORTANT. On most OSX, Linux, and Unix
     Variants the "patch" utility will be installed and accessible by
     default from PHP.
  3. Download and unzip the module into your modules directory like
     any other module
  4. Set the permissions to be writeable by your webserver for the
     following files:

      * includes/menu.inc
      * includes/module.inc
      * includes/theme.inc
      * modules/node/node.module
      * modules/user/user.module
      * sites/all/files or sites/default/files

      THESE PERMISSIONS MUST ALSO BE SET FOR UNINSTALLATION TO SUCCEED
  5. Activate module "Droopal"
  6 For testing and examples, activate module "Droopal Demo OOP", which can
    be compared and contrasted with the module, "Drupal Demo Standard". 

## USAGE INSTRUCTIONS ##

  * Assuming you have activated the module "Droopal Demo OOP",
    go to the url, "droopal/demo/oop". This will demonstrate some of
    the capabilities of Droopal. If that url returns "Page Not Found",
    something has gone wrong, and you should revert your system.
  * For basic code usage examples, have a look at
    "demo/droopal_demo_oop/droopal_demo_oop.module".
  * All classnames are Uppercase/Camelcase.
  * All classnames referring to files in "includes" are prepended with "Lib".
  * All classnames referring to files in "modules" are prepended with "Mod".
  * Here are some examples of how to reference various classes:
    
      - LibBootstrap
          includes/bootstrap.inc
      - LibDatabaseMysqlCommon
          includes/database.mysql-common.inc
      - ModNode
          modules/node/node.module
      - ModUser
          modules/user/user.module

  * You can use the php function "get_class_methods()" to inspect the classes.
  * Anytime a new non-oop function or module is added, you must rebuild
    your Drupal registry, so the new function can be registered with the
    corresponding class.





