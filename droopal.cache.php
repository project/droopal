<?php

/**
 * Handles class caching
 *
 * @author BrendonCrawford
 */

/**
 * reserved words
 *
 */
function droopal_reservedwords() {
  return array(
    'and','or','xor','__FILE__','exception','__LINE__',
    'array','as','break','case','class','const','continue',
    'declare','default','die','do','echo','else','elseif',
    'empty','enddeclare','endfor','endforeach','endif',
    'endswitch','endwhile','eval','exit','extends','for',
    'foreach','function','global','if','include',
    'include_once','isset','list','new','print','require',
    'require_once','return','static','switch','unset',
    'use','var','while','__FUNCTION__','__CLASS__',
    '__METHOD__','final','php_user_filter','interface',
    'implements','instanceof','public','private','protected',
    'abstract','clone','try','catch','throw','cfunction',
    'old_function','this','final','__NAMESPACE__',
    'namespace','goto','__DIR__'
  );
}

/**
 * Initiate cache engine
 *
 */
function droopal_cacheinit() {
  $tpl_class_open =
    "<?php\n" .
    "// THIS FILE IS AUTO-GENERATED.\n" .
    "// ALL EDITS WILL BE LOST WHEN THE REGISTRY IS REBUILT.\n\n" .
    "abstract class %s {\n";
  $tpl_class_close = "}\n\n?>";
  $tpl_func = "  %s static function %s (%s) { return %s(%s); }\n";
  $files = droopal_getfunctions(get_included_files());
  $out_path = file_directory_path() . '/droopal';
  $dirs = scandir($out_path);
  $reserved_words = droopal_reservedwords();
  foreach ($dirs as $old_file) {
    if ($old_file === '.' || $old_file === '..') {
      continue;
    }
    file_delete($out_path . '/' .$old_file);
  }
  foreach ($files as $file => $funcs) {
    if (!$funcs) {
      continue;
    }
    else {
      list($filename, $className, $extension) = droopal_getfilebase($file);
      $prefix = ($extension === 'module' ? 'Mod' : 'Lib');
      $className = $prefix . $className;
      $out = sprintf($tpl_class_open, $className);
      foreach ($funcs as $func => $args) {
        //print_r($args);
        //back out if we dont have the func
        //if (!is_callable($func)) {
        //  continue 0x02;
        //}
        list($methodName, $methodtype) =
          droopal_getmethodname($func, $filename);
        //no reserved words
        if (in_array($methodName, $reserved_words)) {
          $methodName .= '_';
        }
        $vis = ($methodtype === 0x00 ? 'public' : 'protected');
        $args_in = '';
        $args_out = '';
        $i = 0x00;
        while (isset($args[$i])) {
          if ($i > 0x00) {
            $args_in .= ',';
            $args_out .= ',';
          }
          //reference vars
          if ($args[$i][0x00]) {
            $args_in .= '&';
          }
          $args_in .= '$_' . $i;
          $args_out .= '$_' . $i;
          //optional vars
          if ($args[$i][0x01]) {
            $args_in .= '=null';
          }
          $i++;
        }
        $out .= sprintf($tpl_func, $vis, $methodName,
          $args_in, $func, $args_out);
      }
      $out .= $tpl_class_close;
      //Now we have our class.
      //Write it to file
      $out_file = sprintf('%s/%s.class.php', $out_path, $className);
      file_save_data($out, $out_file);
    }
  }
  return true;
}

/**
 * Gets a new method name
 * Does a 1 pass comparison
 *
 */
function droopal_getmethodname($func, $filename) {
  $i = 0x00;
  $j = 0x00;
  $delim = chr(0x5f);
  $type = 0x00;
  $buffer_yes = '';
  $buffer_no = '';
  $found = true;
  $advanced = false;
  while (isset($func{$i})) {
    if ($i === 0x00 && $func{$i} === $delim) {
      $type = 0x01;
      $buffer_yes .= $func{$i};
      $buffer_no .= $func{$i};
      $i++;
    }
    if (isset($filename{$j})) {
      if ($func{$i} !== $filename{$j}) {
        $found = false;
      }
    }
    // account for underscore
    elseif ($found && ($advanced || $func{$i} !== $delim)) {
      $buffer_no .= $func{$i};
      $advanced = true;
    }
    elseif ($found && !$advanced) {
      $advanced = true;
    }
    $buffer_yes .= $func{$i};
    $i++;
    $j++;
  }
  if (!$found || $buffer_no === '') {
    return array($buffer_yes, $type);
  }
  else {
    return array($buffer_no, $type);
  }
}

/**
 * Gets file base
 * Each name gets two passes
 *
 * @author BrendonCrawford
 * @param filename Str
 * @return Array
 *
 */
function droopal_getfilebase($filename) {
  $i = 0x00;
  $out_raw = '';
  $out_proc = '';
  $buffer_raw = array();
  $buffer_proc = array();
  $buffer_raw_len = 0x00;
  $buffer_proc_len = 0x00;
  $buffer_proc_newtok = true;
  $delim = chr(0x5f);
  while (isset($filename{$i})) {
    $ord = ord($filename{$i});
    $char = $filename{$i};
    /**
     * Reset
     */
    if ($ord === 0x2f) {
      $buffer_raw = array();
      $buffer_proc = array();
      $buffer_raw_len = 0x00;
      $buffer_proc_len = 0x00;
      $buffer_raw[$buffer_raw_len] = '';
      $buffer_proc[$buffer_proc_len] = '';
      $buffer_proc_newtok = true;
    }
    /**
     * New Buffer
     */
    elseif ($ord === 0x2e) {
      $buffer_raw[] = '';
      $buffer_raw_len++;
      $buffer_proc[] = '';
      $buffer_proc_len++;
      $buffer_proc_newtok = true;
    }
    /**
     * Advance
     */
    else {
      if (($ord >= 0x41 && $ord <= 0x5a) ||
          ($ord >= 0x61 && $ord <= 0x7a)) {
        if ($buffer_proc_newtok) {
          if ($ord >= 0x61 && $ord <= 0x7a) {
            $buffer_proc[$buffer_proc_len] .= chr($ord - 0x20);
          }
          $buffer_proc_newtok = false;
        }
        else {
          $buffer_proc[$buffer_proc_len] .= $char;
        }
        $buffer_raw[$buffer_raw_len] .= $char;
      }
      elseif ($ord >= 0x30 && $ord <= 0x39) {
        $buffer_raw[$buffer_raw_len] .= $char;
        $buffer_proc_newtok = true;
      }
      elseif ($ord === 0x2d || $ord === 0x5f) {
        $buffer_raw[$buffer_raw_len] .= $delim;
        $buffer_proc_newtok = true;
      }
    }
    $i++;
  }
  /**
   * 2nd Pass, Post Processing
   */
  $out_ext = '';
  for($j = 0x00; $j <= $buffer_raw_len; $j++) {
    if ($j === $buffer_raw_len) {
      $out_ext = $buffer_raw[$j];
    }
    else {
      if ($j > 0x00) {
        $out_raw .= $delim;
      }
      $out_raw .= $buffer_raw[$j];
      $out_proc .= $buffer_proc[$j];
    }
  }
  return array($out_raw, $out_proc, $out_ext);
}


/**
 * We need a fast way to grab all functions in the site.
 * This also returns the amount of arguments per function.
 * This does a one pass parse with no lookahead or lookbehind.
 *
 * @author BrendonCrawford
 * @param files Array
 * @return Assoc
 *
 */
function droopal_getfunctions($files) {
  $trigger = array(
    0x46, 0x55,
    0x4e, 0x43,
    0x54, 0x49,
    0x4f, 0x4e
  );
  $class_trigger = array(
    0x43, 0x4c,
    0x41, 0x53,
    0x53
  );
  $trigger_len = 0x08;
  $class_trigger_len = 0x05;
  $buffer_orig = array();
  $buffer_char_all = array();
  $quote_single = false;
  $quote_double = false;
  foreach ($files as $file) {
    $f = fopen($file, 'r');
    if ($f) {
      $buffer = $buffer_orig;
      $buffer_len = 0x00;
      $buffer_char = '';
      $buffer_char_all[$file] = array();
      $stage = 0x00;
      $is_class = false;
      $class_block_count = 0x00;
      $class_quote_double = false;
      $class_quote_single = false;
      $escape = false;
      while (false !== ($char = fgetc($f))) {
        $ord = ord($char);
        /**
         * Avoid classes
         */
        if ($is_class) {
          // braces cant be between quotes
          //if (!$class_quote_double && !$class_quote_single) {
            if ($ord === 0x7b) {
              $class_block_count++;
            }
            elseif ($ord === 0x7d) {
              $class_block_count--;
              if ($class_block_count === 0x00) {
                $is_class = false;
              }
            }
          //}
          //establish quotes
          //if (!$escape) {
            if ($ord === 0x22) {
              //if (!$class_quote_single) {
                $class_quote_double = !$class_quote_double;
              //}
            }
            elseif ($ord === 0x27) {
              //if (!$class_quote_double) {
                $class_quote_single = !$class_quote_single;
              //}
            }
          //}
        }
        /**
         * Ready for stage commencement
         */
        else {
          /**
           * Stage 0
           */
          if ($stage === 0x00) {
            //newline
            if ($ord === 0x0a || $ord === 0x0d) {
              $stage = 0x01;
            }
          }
          /**
           * Stage 1
           */
          elseif ($stage === 0x01) {
            //whitespace
            if ($ord !== 0x09 && $ord !== 0x0a &&
                $ord !== 0x0d && $ord !== 0x20) {
              $stage = 0x02;
            }
          }
          /**
           * Stage 2
           */
          if ($stage === 0x02) {
            //valid buffer chars
            if (($ord >= 0x41 && $ord <= 0x5a) ||
                ($ord >= 0x61 && $ord <= 0x7a)) {
              $buffer[] = $ord;
              $buffer_len++;
            }
            //class exemption trigger
            elseif ($buffer_len === $class_trigger_len) {
              //whitespace
              if ($ord === 0x09 || $ord === 0x0a ||
                  $ord === 0x0d || $ord === 0x20) {
                $_is_class = true;
                //trigger comparison
                for ($i = 0; $i < $buffer_len; $i++) {
                  if (($buffer[$i] === $class_trigger[$i]) ||
                      ($buffer[$i] === $class_trigger[$i] + 0x20)) {
                    continue;
                  }
                  else {
                    $_is_class = false;
                    break;
                  }
                }
                if ($_is_class) {
                  $is_class = true;
                }
              }
            }
            //trigger
            elseif ($buffer_len === $trigger_len) {
              //whitespace
              if ($ord === 0x09 || $ord === 0x0a ||
                  $ord === 0x0d || $ord === 0x20) {
                $is_trigger = true;
                //trigger comparison
                for ($i = 0; $i < $buffer_len; $i++) {
                  if (($buffer[$i] === $trigger[$i]) ||
                      ($buffer[$i] === $trigger[$i] + 0x20)) {
                    continue;
                  }
                  else {
                    $is_trigger = false;
                    break;
                  }
                }
                if ($is_trigger) {
                  $stage = 0x03;
                }
                //reset
                else {
                  $stage = 0x00;
                }
                $buffer = $buffer_orig;
                $buffer_len = 0x00;
              }
            }
            //reset
            else {
              $buffer = $buffer_orig;
              $buffer_len = 0x00;
              $stage = 0x00;
            }
          }
          /**
           * Stage 3
           */
          if ($stage === 0x03) {
            //whitespace
            if ($ord !== 0x09 && $ord !== 0x0a ||
                $ord !== 0x0d && $ord !== 0x20) {
              $stage = 0x04;
            }
          }
          /**
           * Stage 4
           */
          if ($stage === 0x04) {
            //valid buffer chars
            if (($ord >= 0x30 && $ord <= 0x39) ||
                ($ord >= 0x41 && $ord <= 0x5a) ||
                ($ord >= 0x61 && $ord <= 0x7a) ||
                ($ord === 0x5f)) {
              $buffer[] = $ord;
              $buffer_char .= $char;
              $buffer_len++;
            }
            elseif ($buffer_len > 0x00) {
              //found buffer
              //index:0 is reference vars
              //index:0 is optional vars
              $buffer_char_all[$file][$buffer_char] = array();
              $arg_ref = false;
              $buffer_arg_len = 0x00;
              $stage = 0x05;
            }
          }
          /**
           * Stage 5
           */
          if ($stage === 0x05) {
            // arg:quote exemption
            if ($ord === 0x27) {
              $quote_single = !$quote_single;
            }
            elseif ($ord === 0x22) {
              $quote_double = !$quote_double;
            }
            //not inside quotes
            if (!$quote_single && !$quote_double) {
              // arg:reference
              if ($ord === 0x26) {
                $arg_ref = true;
              }
              // arg:trigger
              if ($ord === 0x24) {
                $buffer_char_all[$file][$buffer_char][$buffer_arg_len] =
                  array(0x00 => false, 0x01 => false);
                if ($arg_ref) {
                  $buffer_char_all[$file][$buffer_char][$buffer_arg_len][0x00] =
                    true;
                  $arg_ref = false;
                }
                $buffer_arg_len++;
              }
              // arg:optional
              if ($ord === 0x3d) {
                $buffer_char_all[$file][$buffer_char][$buffer_arg_len-1][0x01] =
                  true;
              }
              //terminate and continue to next function
              elseif ($ord === 0x7b) {
                $buffer = $buffer_orig;
                $buffer_len = 0x00;
                $buffer_char = '';
                $stage = 0x00;
              }
            }
          }
        }
        /**
         * Post stage checks
         */
        if ($ord === 0x5c) {
          $escape = true;
        }
        else {
          $escape = false;
        }
      }
      fclose($f);
    }
  }
  return $buffer_char_all;
}



?>